import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import RobotSongsView from '../views/RobotSongsView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/robot-songs',
      name: 'robotSongs',
      component: RobotSongsView
    },
  ]
})

export default router
