import { createApp } from 'vue'
import { createVuetify } from 'vuetify/lib/framework.mjs'
import 'vuetify/styles'
import App from './App.vue'
import './assets/main.css'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'

const vuetify = createVuetify({
  components,
  directives,
})
const app = createApp(App)
app.use(vuetify)

app.mount("#app")
